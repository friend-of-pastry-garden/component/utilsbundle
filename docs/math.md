Composant Math 
--
Le module Math a pour objectif de fournir un ensemble d'outils orienté sur l'aide à la prévision.

Recherche des courbes de tendances 
= 
L'analyse des courbes de tendances est une tâche qui à l'aide d'un graphe est assez simple à appréhender. Pour les besoins d'automatisation, programmer un robot pour effectuer cet opération est indispensable pour effectuer des traitements de masse. Pour ce faire, il y'a nécessité de proposer un modèle mathématique nous garantissant l'exactitude des résultats ainsi que l'implémentation associée.
L'approche brute de recherche nécessite de tester l'ensemble des triplets d'une fonction. La compléxité associée est par conséquent $\Phi (n^{3})$. La cible va donc consister à trouver une solution réduisant la compléxité.

Modèle mathématique 
==
On va proposer un modèle alternatif permettant de positionner la "hauteur" d'un point u dans un triplet (x,y,z) d'une fonction f pour le rattacher à la définition conventionnelle de concavité. Une fois fait, on va proposer un théorème d'extension de concavité qui nous permettra d'implémenter la recherche linéaire de localité concave.

Modèle d'équivalence à la définition classique de concavité 
===
**Définition** : On appelle *fonction de prolongement de f sur (x,y)* la fonction $\phi^f_{x,y}(u) = P_{x,y}(u-x)+f(u) $ avec $x,y,u \in Dom(f)$ et $P_{x,y} = \frac{f(x)-f(y)}{x-y}$

**Théorème** : $P_{x,y}=P_{y,x}$ et $\phi^f_{x,y}=\phi^f_{y,x}$

**Définition** : On appelle *fonction comparative de prolongement de f sur (x,y,z)* la fonction $\omega^f_{x,y,z} = \phi^f_{x,y} - \phi^f_{y,z}$

**Théorème** : $\omega^f_{x,y,z} = -\omega^f_{z,y,x}$

**Théorème** : $\omega^f_{x,y,z} \ge 0 = \begin{cases} P_{x,y} \ge P_{y,z} \text{ si } x > y \\ P_{x,y} \le P_{y,z} \text{ si } x < y\end{cases} $

**Corollaire (loi des 3 pentes généralisées en concavité)** : Soit $x,y,z \in Dom(f)$. Si $\omega^f_{y,x,z} \ge 0$ et $\omega^f_{y,z,x} \ge 0$ alors 
$\begin{cases} 
P_{y,z} \le P_{x,z} \le P_{x,y} \text{ si }x < y < z  \\
min(P_{y,z},P_{y,x}) < P_{x,z} \text{ si }y < x < z \\
P_{x,z} \le max(P_{z,y},P_{x,y}) \text{ si }x < z < y
\end{cases}$ 

**Théorème** : Soit un intervalle $I \in Dom(f)$, on a l'équivalence suivante : 
* f est concave sur I
* $\forall x,y,z \in I: x < y < z. \omega^f_{x,y,z} \ge 0 \cap \omega^f_{x,z,y} \ge 0$

**Théorème d'extension de concavité** : Soit un intervalle $I \in Dom(f)$ et $c \in Dom(f) : c > max(I)$. 

Si f est concave sur $I$ et si $P_{min(I),max(I)} \le max(P_{max(I),c},P_{min(I),c})$, alors f est concave sur $I \cup \{c\}$

Recherche des fonctions d'encadrement 
===

**Définition** : Soit $I_1, I_2 \in Dom(f)$, f distinctement concave sur les intervalles $I_1$ et $I_2$. On appelle couple de majoration M à $I_1$ et $I_2$ : 

$\{(x,y)| x \in I_1, y \in I_2 . \forall u \in I_1 \cup I_2 . \omega^f_{u,x,y} \le 0 \cap \omega^f_{u,y,x} \le 0\}$

**Théorème** : Soit (u,v) le couple de majoration M à $I_1$ et $I_2$, alors $min(P_{u-1,u}, P_{u-1, v}, P_{u+1,v}) \ge P_{u,v} \ge P_{u,u+1}$ et $P_{v-1,v} \ge P_{u,v} \ge max(P_{u,v-1}, P_{u,v+1}, P_{v,v+1})$

*Preuve* : 

* Etape 1 : on considère que v est connu, l'objectif est de déterminer u. L'ensemble des éléments x doivent être sous la droite de prolongement à (u,v).

Il vient les deux situations suivantes : 
- Si $x < u < v$, alors $min(P_{x,u}, P_{x,v}) \ge P_{u,v}$
- Si $u < x < v$, alors $P_{x,v} \ge P_{u,v} \ge P_{u,x}$

On déduit $min(P_{u-1,u}, P_{u-1, v}, P_{u+1,v}) \ge P_{u,v} \ge P_{u,u+1}$

* Etape 2 : on considère que u est connu, l'objectif est de déterminer v. L'ensemble des éléments x doivent être sous la droite de prolongement à (u,v).

Il vient les deux situations suivantes : 
- Si $u < x < v$, alors $P_{x,v} \ge P_{u,v} \ge P_{u,x}$
- Si $u < v < x$, alors $max(P_{x,v},P_{u,x}) \ge P_{u,v}$

On déduit $P_{v-1,v} \ge P_{u,v} \ge max(P_{u,v-1}, P_{u,v+1}, P_{v,v+1})$
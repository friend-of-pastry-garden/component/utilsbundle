<?php

namespace FOPG\Component\UtilsBundle\Tests;

use FOPG\Component\UtilsBundle\Math\Segment;
use FOPG\Component\UtilsBundle\Math\Interval;
use FOPG\Component\UtilsBundle\Test\TestCase;

class MathTest extends TestCase
{
    const SECTION_HEADER = '[Math]';

    public function testInterval(): void {
      $data=[28 =>1024];
      $mem=0;
      $x=0;
      for($i=0;$i<10;$i++) {
        $mem = pow(2,$i);
        $x=$i;
        $data[$x]=$mem;
      }
      for($i=1;$i<9;$i++) {
        $mem-=pow(2,$i);
        $x=$x+1;
        $data[$x]=$mem;
      }
      for($i=1;$i<=9;$i++) {
        $mem=pow(2,$i);
        $x=$x+1;
        $data[$x]=$mem;
      }

      $this
        ->given(
          description: self::SECTION_HEADER." Gestion des intervalles",
          data: $data
        )
        ->when(
          description: "J'analyse une intervalle donnée",
          callback: function(array $data, Interval &$interval=null) {
            $interval = new Interval($data);
          }
        )
        ->then(
          description: "Je retrouve les portions concaves et convexes de l'intervalle",
          callback: function(Interval $interval) {
            $sections = $interval->getSegments();
            $check = count($sections)===3;
            $check2=false;
            if(!empty($sections[0])) {
              $first=$sections[0]->first();
              $check2=($sections[0]->first()==0 && $sections[0]->len()==10 && $sections[0]->getAttribute('max')==512);
            }
            return [$check,$check2];
          },
          result: [true, true]
        )
      ;

      $tab = self::generate_periodic_function(2000);
      $this
        ->given(
          description: "Evaluation des bornes sur un intervalle donné",
          tab: $tab
        )
        ->when(
          description: "J'analyse la fonction générée",
          callback: function(array $tab, Interval &$interval=null) {
            $interval=new Interval($tab);
          }
        )
        ->then(
          description: "Les fonctions de majoration doivent bien encadrées la fonction",
          callback: function(array $tab, Interval $interval) {
            $size = 500;
            $data=$interval->getLimits($tab,$size);
            $success = 0;
            $cpt = 0;
            foreach(array_merge($data['convexes'],$data['concaves']) as $item) {
              $s = $item['segment'];
              $min = $item['first']->last();
              if(empty($item['last'])||empty($s))
                continue;
              $max = $item['last']->first();
              $forbiddenPuncts=[$s->first(),$s->last()];
              if($max-$min > ($size*.8)) {
                for($i=$min+1;$i<$max;$i++) {
                  if(!in_array($i,$forbiddenPuncts)) {
                    $tmp = $s->isConvexe() ? (false === $s->increase($i,$tab[$i])) : (true === $s->increase($i,$tab[$i]));
                    if(true === $tmp)
                      $success++;
                    $cpt++;
                  }
                }
              }
            }
            return ($success/$cpt)>.97;
          },
          result: true
        )
      ;
    }

    private static function generate_periodic_function(int $length): array
    {
      $tab=[];
      $pos=0;
      $val = 100;
      $rate=1.01;
      for($i=0;$i<$length;$i++) {
        $pos+=rand(1,1);
        $rate=1+(rand(-30,30)/1000);
        $val*=$rate;
        $tab[$pos]=$val;
      }
      return $tab;
    }

    public function testSegment(): void {

      $this
        ->given(
          description: self::SECTION_HEADER." Gestion de segment",
        )
        ->when(
          description: "Je créé un segment [a..b]",
          callback: function(Segment &$segment=null) {
            $segment = new Segment(
              x:1,
              xVal:5,
              y:2,
              yVal:7
            );
          }
        )
        ->then(
          description: "La pente doit être trouvée",
          callback: function(Segment $segment) {
            return $segment->getSlope();
          },
          result: (float)2
        )
        ->andThen(
          description: "Je dois pouvoir effectuer un prolongement",
          callback: function(Segment $segment) {
            return $segment->getVal(offset: 3);
          },
          result: (float)9
        )
        ->andThen(
          description: "Je peux estimer si un point est au dessus du segment",
          callback: function(Segment $segment) {
            $check1 = $segment->increase(offset: 3, val: 9.01);
            $check2 = $segment->increase(offset: 3, val: 8.99);
            $check3 = $segment->increase(offset: 1.5, val: 6.01);
            $check4 = $segment->increase(offset: 1.5, val: 5.99);
            $check5 = $segment->increase(offset: 0, val: 3.01);
            $check6 = $segment->increase(offset: 0, val: 2.99);
            return [$check1,$check2,$check3,$check4,$check5,$check6];
          },
          result: [true,false,true,false,true,false]
        )
      ;
    }
}

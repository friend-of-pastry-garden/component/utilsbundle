<?php

namespace FOPG\Component\UtilsBundle\Filesystem;

use FOPG\Component\UtilsBundle\Exception\InvalidFilenameException;
use FOPG\Component\UtilsBundle\Collection\Collection;

class File extends AbstractFso
{
  private ?string $_filename = null;
  private ?string $_extension = null;

  /**
   * @param string $folder
   * @return array<int, File>
   */
  public static function get_files(string $folder): array {
    $output=[];
    $folder = preg_replace("/\/+$/","", $folder);
    if(is_dir($folder)) {
      $files = scandir($folder);
      foreach($files as $file) {
        if(is_file($folder.'/'.$file))
          $output[]=new File($folder.'/'.$file);
      }
    }
    return $output;
  }

  /**
   * Renvoi du contenu d'un fichier
   *
   * @return string
   */
  public function getContent(): string {
    /** @var string $file */
    $file = (string)$this;
    return file_get_contents($file);
  }

  /**
   * Renvoi de l'alphabet associé au contenu
   *
   */
  public function getLetters(): Alphabet {
    /** @var string $content */
    $content = $this->getContent();
    /** @var array<int, char> $tmp */
    $tmp = mb_str_split($content);
    /** @var int $len */
    $len = count($tmp);
    /** @var Alphabet $alphabet */
    $alphabet = new Alphabet($len);
    /** @var array<int, string> $letters */
    $letters = array_unique($tmp);
    /** @var string $char */
    $cpt=0;
    foreach($letters as $char) {
      $i=0;
      foreach($tmp as $tChar)
        if($tChar === $char)
          $i++;
      $alphabet->add($char, $i);
      $cpt+=$i;
    }
    return $alphabet;
  }

  /**
   * Renvoi de segment d'alphabet pour un dossier cible avec leurs fréquences d'apparition associées
   *
   * @return array<string, float>
   */
  public static function get_alphabet(string $folder, int $segments=1): array {
    $frequencies = [];
    /** @var array<int, File> $files */
    $files = File::get_files($folder);
    $alphabet = new Alphabet();
    foreach($files as $file) {
      $newAlphabet = $file->getLetters();
      $alphabet->append($newAlphabet);
    }
    $frequencies = $alphabet->toArray($segments);
    return $frequencies;
  }

  public function __toString(): string {
    return $this->getDirectory().'/'.$this->getBasename();
  }

  public function __construct(string $filename) {
    if(preg_match(self::REGEXP_DIRECTORY, $filename, $matches)) {
      $this->_extension = $matches['extension'];
      $this->_filename  = $matches['filename'];
      $this->setDirectory($matches['directory']);
    }
    else
      throw new InvalidFilenameException($filename);
  }

  public function getBasename(): string {
    return $this->_filename.'.'.$this->_extension;
  }

  /**
   * Fonction de copie de document
   *
   * @param string $filename Fichier source à copier
   * @param string $newFilename Fichier destination
   * @param bool $force Option d'écrasement si la destination existe
   * @return bool L'action de copie s'est t'elle bien opérée ?
   */
  public static function copy(string $filename, string $newFilename, bool $force=false): bool {

    if(!file_exists($filename))
      return false;

    if(file_exists($newFilename) && (true===$force))
      @unlink($newFilename);

    if(file_exists($newFilename))
      return false;

    @copy($filename, $newFilename);
    return file_exists($newFilename);
  }
}

<?php

namespace FOPG\Component\UtilsBundle\Filesystem;

use FOPG\Component\UtilsBundle\Collection\Collection;

class Alphabet {
  private ?Collection $_collection=null;
  private int $_len=0;

  public function getLetters(): array {
    $tab=[];
    foreach($this->_collection as $item)
      $tab[]=$item;
    return $tab;
  }

  public function add(mixed $letter, int $length): self {
    $obj = new Letter($letter, $length);
    $this->_collection->add($obj);
    return $this;
  }

  public function getLength(): int {
    return $this->_len;
  }

  public function __construct(int $length=0) {
    $this->_collection = new Collection(
      [],
      function($index, Letter $item): int { return $item->getLength(); },
      function(int $lengthA, int $lengthB): bool { return $lengthA > $lengthB; }
    );
    $this->_len = $length;
  }

  public function contains(Letter $tObj): ?Letter {
    /** @var Letter $lObj */
    foreach($this->getLetters() as $index => $lObj) {
      if($tObj->getTerm() === $lObj->getTerm()) {
        return $lObj;
      }
    }
    return null;
  }

  /**
   * Renvoi du tableau de l'alphabet découpé en section de densité équivalentes
   *
   * @param int $sections
   * @return array<array>
   */
  public function toArray(int $sections=1): array {
    $tab = $this->_collection->toArray();
    foreach($tab as $index => $blob) {
      $tab[$index]['length']=$blob['data']->getLength();
      $tab[$index]['data']=$blob['data']->getTerm();
    }

    $tSections = $this->split($tab, $sections);
    return $tSections;
  }

  public function split(array $tab, int $sections): array {
    $tSections=[];

    for($i=0;$i<$sections;$i++) {
      $firstLetter = $tab[$i]['data'];
      $firstlength = $tab[$i]['length'];
      $tSections[$i]=['alphabet' => [$firstLetter],'length' => $firstlength, 'rate' => 0];
      unset($tab[$i]);
    }
    self::recursive_split($tab, $tSections);

    foreach($tSections as $index => $data)
      $tSections[$index]['rate']=($tSections[$index]['length']/$this->getLength());
      
    return $tSections;
  }

  private static function recursive_split(array $tab, array &$tSections): void {

    if(0 === count($tab))
      return;

    $data = array_shift($tab);
    $aggLength = $data['length'];
    $aggData = $data['data'];
    $min = null;
    $pos = null;
    foreach($tSections as $index => $tSection) {
      $length = $tSection['length'];
      if(null === $min) {
        $min = $length;
        $pos = $index;
      }
      if($min > $length) {
        $min = $length;
        $pos = $index;
      }
    }
    $tSections[$pos]['alphabet'][]=$aggData;
    $tSections[$pos]['length']+=$aggLength;

    self::recursive_split($tab, $tSections);
  }

  public function append(Alphabet $alphabet): self {
    $this->_len+=$alphabet->getLength();
    foreach($alphabet->getLetters() as $lObj) {
      if(null !== ($tObj = $this->contains($lObj)))
        $tObj->increaseLength($lObj->getLength());
      else
        $this->_collection->add($lObj);
    }
    $this->_collection->heapSort();

    return $this;
  }

}

<?php

namespace FOPG\Component\UtilsBundle\Filesystem;

use FOPG\Component\UtilsBundle\Contracts\FilesystemInterface;
use FOPG\Component\UtilsBundle\Exception\InvalidDirectoryException;

abstract class AbstractFso implements FilesystemInterface
{
  private ?string $_directory=null;

  public function getDirectory(): ?string {
    return $this->_directory;
  }

  public function setDirectory(string $directory): self {
    $directory = preg_replace("/\/$/","", $directory);
    if(file_exists($directory) && is_dir($directory))
      $this->_directory = $directory;
    else
      throw new InvalidDirectoryException($directory);
    return $this;
  }
}

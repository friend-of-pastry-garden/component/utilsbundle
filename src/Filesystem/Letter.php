<?php

namespace FOPG\Component\UtilsBundle\Filesystem;

class Letter {

  private mixed $_term = null;
  private int $_length = 0;

  public function __construct(mixed $term, int $length) {
    $this->_term = $term;
    $this->_length = $length;
  }

  public function getTerm(): mixed {
    return $this->_term;
  }

  public function getLength(): int {
    return $this->_length;
  }

  public function increaseLength(int $newLength): self {
    $this->_length+=$newLength;
    return $this;
  }
}

<?php

namespace FOPG\Component\UtilsBundle\Math;

class Segment {
  private ?bool $_direction=null;
  private array $_garbage=[];
  private float $_x;
  private float $_xVal;
  private float $_y;
  private float $_yVal;
  public function setAttribute(string $key,mixed $val): self { $this->_garbage[$key]=$val; return $this; }
  public function getAttribute(string $key): mixed { return $this->_garbage[$key]??null; }
  public function setDirection(?bool $direction): self { $this->_direction=$direction; return $this; }
  public function getDirection(): ?bool { return $this->_direction; }
  public function first(): float { return $this->_x; }
  public function last(): float { return $this->_y; }
  public function len(): float { return $this->_y-$this->_x+1; }
  public function __construct(float $x, float $xVal, float $y, float $yVal)
  {
    if($x==$y)
      throw new \Exception('Invalid offset: X can not be equals to Y');

    if($x > $y) {
      $tmp = $x;
      $x = $y;
      $y = $tmp;
      $tmp = $xVal;
      $xVal = $yVal;
      $yVal = $tmp;
    }
    $this->_x=$x;
    $this->_y=$y;
    $this->_xVal=$xVal;
    $this->_yVal=$yVal;
  }

  public function getSlope(): float
  {
    $a = $this->_yVal-$this->_xVal;
    $b = $this->_y-$this->_x;
    return $a/$b;
  }

  public function getVal(float $offset): float
  {
    $k = $this->getSlope();
    return $k*($offset-$this->_x)+$this->_xVal;
  }

  public function isConcave(): ?bool {
    if(null === $this->getDirection())
      return null;
    return !$this->getDirection();
  }

  public function isConvexe(): ?bool {
    if(null === $this->getDirection())
      return null;
    return $this->getDirection();
  }

  public function increase(float $offset, float $val): bool
  {
    $fac = new Segment(x: $this->_x,xVal: $this->_xVal,y: $offset,yVal: $val);
    $fbc = new Segment(x: $this->_y,xVal: $this->_yVal,y: $offset,yVal: $val);
    $pac = $fac->getSlope();
    $pbc = $fbc->getSlope();
    $firstCriteria = (($offset>$this->_x)&&($pac>=$this->getSlope()))||(($offset<$this->_x)&&($pac<$this->getSlope()));
    $secondCriteria = (($offset>$this->_y)&&($pbc>=$this->getSlope()))||(($offset<$this->_y)&&($pbc<$this->getSlope()));
    return ($firstCriteria&&$secondCriteria);
  }
}

<?php

namespace FOPG\Component\UtilsBundle\Math;

class Interval {
  private array $_data=[];
  public function __construct(array $data) {
    ksort($data, SORT_NUMERIC);
    $this->_data=$data;
  }

  public static function array_append(array &$tab, ?array $new): void {
    if(null === $new)
      return;
    $z = key($new);
    $tab[$z]=$new[$z];
    ksort($tab, SORT_NUMERIC);
  }

  public function getSegments(): array {
    $tab=$this->_data;
    $sections = [];
    while(null !== ($section=self::extract_section($tab)))
      $sections[]=$section;
    return $sections;
  }

  public function getFirstConcave(): ?Segment {
    $segments = $this->getSegments();
    $tab = [];
    foreach($segments as $segment)
      if(true === $segment->isConcave())
        return $segment;
    return null;
  }

  public function getFirstConvexe(): ?Segment {
    $segments = $this->getSegments();
    $tab = [];
    foreach($segments as $segment)
      if(true === $segment->isConvexe())
        return $segment;
    return null;
  }

  public function getConcaves(): array {
    $segments = $this->getSegments();
    $tab = [];
    foreach($segments as $segment)
      if(true === $segment->isConcave())
        $tab[]=$segment;
    return $tab;
  }

  public function getConvexes(): array {
    $segments = $this->getSegments();
    $tab = [];
    foreach($segments as $segment)
      if(true === $segment->isConvexe())
        $tab[]=$segment;
    return $tab;
  }

  private static function get_majoration_function(Segment $s, Segment $s2, array $tab): Segment
  {
    /** @var bool $isConcave */
    $isConcave = $s->isConcave();
    $u = (int)$s->first()+1;
    $v = (int)$s2->first();
    for(;$u<$s->last();$u++)
    {
      $pu0_u = ($tab[$u]-$tab[$u-1]);
      $pu0_v = ($tab[$v]-$tab[$u-1])/($v-$u+1);
      $pv_u2 = ($tab[$v]-$tab[$u+1])/($v-$u-1);

      $puv = ($tab[$v]-$tab[$u])/($v-$u);
      $pu_u2 = ($tab[$u+1]-$tab[$u]);

      $fCheck = (min($pu0_u, $pu0_v, $pv_u2) >= $puv);
      $sCheck = ($puv >= $pu_u2);
      if(false === $isConcave) {
        $fCheck = (min($pu0_u, $pu0_v, $pv_u2) <= $puv);
        $sCheck = ($puv <= $pu_u2);
      }
      if($fCheck && $sCheck)
        break;
    }

    $v = $v+1;
    for(;$v<$s2->last();$v++)
    {
      $pv0_v = ($tab[$v]-$tab[$v-1]);

      $puv = ($tab[$v]-$tab[$u])/($v-$u);

      $pv0_u = ($tab[$u]-$tab[$v-1])/($u-$v+1);
      $pv2_u = ($tab[$u]-$tab[$v+1])/($u-$v-1);
      $pv_v2 = ($tab[$v+1]-$tab[$v]);

      $fCheck = ($pv0_v >= $puv);
      $sCheck = ($puv >= max($pv0_u,$pv2_u,$pv_v2));
      if(false === $isConcave) {
        $fCheck = ($pv0_v <= $puv);
        $sCheck = ($puv <= max($pv0_u,$pv2_u,$pv_v2));
      }
      if($fCheck && $sCheck)
        break;
    }

    $segment = new Segment($u, $tab[$u],$v, $tab[$v]);
    $segment->setDirection(!$s->isConvexe());
    return $segment;
  }

  private function getExtremumBySegment(Segment $offsetSegment, int $maxsize=250, array $tab): ?array {
    $output=[
      'first' => $offsetSegment,
      'last' => null,
      'maximize' => null,
      'minimize' => null,
      'segment' => null,
    ];
    if(true === $offsetSegment->isConcave()) {
      /** @var array<int, Segment> $concaves */
      $concaves = $this->getConcaves();
      /** @var float $u index de l'élément minimal de l'amplitude à analyser */
      $u = array_search($offsetSegment, $concaves);
      $min = $offsetSegment->first();
      /** @var float $w index de l'élément maximal de l'amplitude à analyser */
      for($w=$u+1,$amplitude = 0;$amplitude<$maxsize;$w++)
        $amplitude = !empty($concaves[$w]) ? $concaves[$w]->last()-$min : $maxsize;
      $w=max($w-2,$u);
      $output['last']=$concaves[$w];
      /** @var float $maxSlope */
      $maxSlope = -9999999;
      /** @var $v index de l'élémént maximisant la pente */
      $v = $u;
      for($i=$u+1;$i<$w;$i++) {
        $segment = self::get_majoration_function(s: $offsetSegment,s2: $concaves[$i],tab: $tab);
        if($segment->getSlope() > $maxSlope) {
          $maxSlope = $segment->getSlope();
          $v = $i;
          $output['maximize'] =$concaves[$v];
          $output['segment'] = $segment;
        }
      }
    }
    if(true === $offsetSegment->isConvexe()) {
      /** @var array<int, Segment> $convexes */
      $convexes = $this->getConvexes();
      /** @var float $u index de l'élément minimal de l'amplitude à analyser */
      $u = array_search($offsetSegment, $convexes);
      $min = $offsetSegment->first();
      /** @var float $w index de l'élément maximal de l'amplitude à analyser */
      for($w=$u+1,$amplitude = 0;$amplitude<$maxsize;$w++)
        $amplitude = !empty($convexes[$w]) ? $convexes[$w]->last()-$min : $maxsize;
      $w=max($w-2,$u);
      $output['last']=$convexes[$w];
      /** @var float $minSlope */
      $minSlope = 9999999;
      /** @var $v index de l'élémént minimisant la pente */
      $v = $u;
      for($i=$u+1;$i<$w;$i++) {
        $segment = self::get_majoration_function(s: $offsetSegment,s2: $convexes[$i],tab: $tab);
        if($segment->getSlope() < $minSlope) {
          $minSlope = $segment->getSlope();
          $v = $i;
          $output['minimize'] = $convexes[$v];
          $output['segment'] = $segment;
        }
      }
    }
    return $output;
  }
  /**
   * @author yanroussel
   *
   *        Detection of relative limits of curb
   *        Rate of error is less than 10%
   */
  public function getLimits(array $tab, int $maxsize=250): array {
    $output=[
      'convexes' => [],
      'concaves' => [],
    ];
    /** @var ?Segment $concave */
    $concave = $this->getFirstConcave();
    /** @var ?Segment $convexe */
    $convexe = $this->getFirstConvexe();
    do {
      $tmp = $this->getExtremumBySegment(offsetSegment: $concave, maxsize: $maxsize, tab: $tab);
      $concave = $tmp['maximize'];
      $output['concaves'][] = $tmp;
    }while($concave);
    do {
      $tmp = $this->getExtremumBySegment(offsetSegment: $convexe, maxsize: $maxsize, tab: $tab);
      $convexe = $tmp['minimize'];
      $output['convexes'][] = $tmp;
    }while($convexe);
    return $output;
  }

  public static function extract_section(array &$tab): ?Segment
  {
    if(0===count($tab))
      return null;
    $first = self::extract_first($tab);
    $second = self::extract_first($tab);
    $third = self::extract_first($tab);
    $a = (null!==$first) ? key($first) : null;
    $b = (null !== $second) ? key($second) : null;
    $c = (null !== $third) ? key($third) : null;
    if(null === $b)
      return null;
    $max = max($first[$a],$second[$b]);
    $min = min($first[$a],$second[$b]);
    $section = new Segment($a, $first[$a], $b, $second[$b]);
    $section->setAttribute('max',$max);
    $section->setAttribute('offsetMax', ($max==$first[$a])?$a:$b);
    $section->setAttribute('min',$min);
    $section->setAttribute('offsetMin', ($min==$first[$a])?$a:$b);
    $section->setDirection(null);
    if(null === $c)
      return $section;
    /** bool $direction */
    $direction=$section->increase($c,$third[$c]);
    /** Section $section */
    do {
      $max=max($section->getAttribute('max'), $third[$c]);
      $min=min($section->getAttribute('min'), $third[$c]);
      $offsetMax = $section->getAttribute('offsetMax');
      $offsetMin = $section->getAttribute('offsetMin');

      $section = new Segment($a, $first[$a], $c, $third[$c]);
      $section->setAttribute('max',$max);
      $section->setAttribute('min',$min);
      $section->setAttribute('offsetMax',($max==$third[$c])?$c:$offsetMax);
      $section->setAttribute('offsetMin',($min==$third[$c])?$c:$offsetMin);
      $section->setDirection($direction);
      $third = self::extract_first($tab);
      $c = (null !== $third) ? key($third) : null;
    }
    while((null!==$c)&&($section->increase($c, $third[$c]) === $direction));
    self::array_append($tab, $third);
    return $section;
  }

  public static function extract_first(array &$tab): ?array
  {
    if(0===count($tab))
      return null;
    list($k) = array_keys($tab);
    $r = array($k=>$tab[$k]);
    unset($tab[$k]);
    return $r;
  }
}
